## Project Testing Setup

- Place image datasets in`image_folder` and generated files will be generated in `output_folder`.
- Place a python script inside scripts folder e.g general_workflow.py
- Run the following command to start running the script:

```
$ make run file_name=general_workflow.py
```

- Make sure deleting everything inside `output_folder` before running the script.
